---
type: Location
location: "[[Shieldtown]]"
---

# Description
Druid Grove outside of [[Shieldtown]]
# History
[[Melf]] took control and cursed the [[Oakfather]], a big tree at the center of the grove

# Population
```dataview
LIST
FROM "Notes/NPCs"
WHERE location = [[]]
```

# Sessions where we started here
```dataview
LIST
FROM "Sessions"
WHERE location = [[]]
```
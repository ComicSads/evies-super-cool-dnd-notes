---
location: "[[../Shieldtown|Shieldtown]]"
type: Tavern
---
Located just outside of [Shieldtown](../Shieldtown.md)
Owned and operated by [Jerry](../../NPCs/Probably%20Not%20Important%20Anymore/Jerry.md), Sara's group stopped here and [Zeal](../../../Players/Zeal.md) ended up killing a man in plate amour in an honor duel.
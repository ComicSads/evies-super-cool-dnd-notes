---
type: City
---

# Description
A haphazard wood wall surrounds the town. A few roofs are able to peak over the wall. A rib cage of a [titan](../../Other/titans.md) with moss and stuff surrounds the center of the town.
Governed by [Bassen Ninsk](../../NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md), the magistrate.
# History
[Bassen Ninsk](../../NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md) instated a quarantine on the town a couple weeks before the party arrived. Later on he hired them to clear out [The Crypt](Probably%20unimportant%20to%20me%20specifically/The%20Crypt%20in%20East%20Grove.md) but as of yet have not paid them for their services.
Later on [Melf](../../NPCs/Melf.md) resurrected the [titan](../../Other/titans.md) resulting in the death of most of the [Population](East%20Grove.md#Population) and the total destruction of the town.
# Contained locations

```dataview
TABLE WITHOUT ID
file.link as "Name",
type AS "Type"
FROM "Notes/Locations"
WHERE location = [[]]
```

# Population
```dataview
LIST
FROM "Notes/NPCs"
WHERE location = [[]]
```

# Sessions where we started here
```dataview
LIST
FROM "Sessions"
WHERE location = [[]]
```

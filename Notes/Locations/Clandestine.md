---
type: City
---

# Description
Blah blah
# History
Blah blah
# Contained locations

```dataview
TABLE WITHOUT ID
file.link as "Name",
type AS "Type"
FROM "Notes/Locations"
WHERE location = [[]]
```

# Population
```dataview
LIST
FROM "Notes/NPCs"
WHERE location = [[]]
```

# Sessions where we started here
```dataview
LIST
FROM "Sessions"
WHERE location = [[]]
```
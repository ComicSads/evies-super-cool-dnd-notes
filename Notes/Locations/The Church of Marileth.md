---
aliases: 
location: "[[Shieldtown]]"
type: Church
---
Worships the goddess of [Marileth](The%20Church%20of%20Marileth.md#Marileth)
Was ran by [Argash](Argash.md) before his defeat and is now headed by [Alavia](Alavia.md)
# Location
Located an hour outside of [Shieldtown](Shieldtown.md)
# Marileth
Teaches absolute justice absolutely.
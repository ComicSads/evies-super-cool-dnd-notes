---
location: "[[Shieldtown]]"
type: School
---
School where [Baloc](Baloc.md) went and studied a couple hundred years ago. Had multiple students disappear over a period of a few months before the party arrived. [Stradavarius Gael](Stradavarius%20Gael.md) tried to figure out the reason for their disappearance while [Barnaby Hagglesworth](Barnaby%20Hagglesworth.md) just didn't do anything
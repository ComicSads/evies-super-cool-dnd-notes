---
location: "[[../Locations/Shieldtown|Shieldtown]]"
type: Guild
---
Headquartered in [Shieldtown](../Locations/Shieldtown.md). [Baloc](../../Players/Baloc.md) worked here for a time before leaving for unknown reasons.
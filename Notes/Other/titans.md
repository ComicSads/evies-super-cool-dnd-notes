---
aliases:
  - titan
---
Hulking gigantic creatures that used to live in the continent of [Mahlus](../Mahlus.md) thousands of years ago. Their corpses litter the landscape now forming landmarks which are considered unremarkable to the people who live there.

One was resurrected by [Melf](../NPCs/Melf.md) in [East Grove](../Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md), with likely more in [Shieldtown](../Locations/Shieldtown.md) and other locations.
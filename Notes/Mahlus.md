The super continent the game takes place on,mostly an unorganized system of nobles with serfs and stuff, not a major economy.
Interconnected travel is rare leaving people suspicious of strangers, and individual towns functionally act as states. The roads are dangerous and inhospitable and disease is rampant.

# History
Mahlus previously had giant humanoids known as the [titans](Other/titans.md) living here and their corpses still dot the landscape, which are now mostly unremarkable landmarks.

---
location:
---

Friend of [Mia](Mia.md) and [Lexalia](Lexalia.md), daughter of [Locke](Locke.md). Half elf and orc. Is traveling with the caravan in Sara's group now currently after [Locke](Locke.md)'s passing. Is now under the care of [Lyra](Lyra.md) with [Betsy](Betsy.md) as a pet
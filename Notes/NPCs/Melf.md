---
tags:
  - evil
aliases:
  - Gotrek
alive: true
---
Owner of [Gotrek's Magnificent Wares](../Locations/Probably%20unimportant%20to%20me%20specifically/Gotrek's%20Magnificent%20Wares.md), before abandoning just before [The Magistrate's](Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md)'s quarantine. Likely killed the real Gotrek who lived in [Shieldtown](../Locations/Shieldtown.md). Taught [Jericho](Probably%20Not%20Important%20Anymore/Jericho.md) how to do necromancer stuff and funded his operation for a time.
---
date: 2024-05-21
location:
---
# Past Session Recap
- Librarian name is [Maeve Librarian](Maeve%20Librarian.md)
# This Session
- [Baloc](../Players/Baloc.md) walks into the library
- Finds a intriguing book in the chest that's been magically locked and needs to find the lock spell
- Starts decoding it
- [Vi](Vi.md) says decay drove the Grove Wardens mad
- They plant people not trees
- [Colin](../Players/Colin.md) sand [Dalia](../Players/Dalia.md) see a very wide tree with a pained face that is horribly decayed
- Large decayed treeants around, no leaves, corpses laying around and in the treeant hands
- Small black cat with green eyes comes up
- turns into [Tharpa](Tharpa.md)
- Decay started about a month ago
- Only person who would have known why this is happening is the archdruid who is dead
- [Oakfather](Oakfather.md) (Oakfather) is sick and the heart of [Ardweld](Ardweld.md)
	- Probably not poison
- Find a song in the tome [Baloc](../Players/Baloc.md) decoded
	- Old enough that it should not be possible for it to still be around
- [Tharpa](Tharpa.md) leads us to where the rat message we sent by doing druid shit
- We find footprints and start following them
- We find an Elk and it turns into a druid dwarf who took the message and knew [Melf](../Notes/NPCs/Melf.md), says he joined the grove
- Tells us to find the other 2 remaining druids
	- Narfe
	- Arilla
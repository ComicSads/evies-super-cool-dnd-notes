---
date: 2024-02-15
location: "[[../Notes/Locations/Probably unimportant to me specifically/East Grove|East Grove]]"
---
# This Session
- [John](../Players/Faurgar.md) gets a point of inspiration.
- We go to [Bassen Ninsk](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md)'s office
	- Players who aren't here go take the supplies from [Locke](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Locke.md)'s shop to the caravan
- It's the same guard as last time and we get let in
- Super duper fancy mansion
- His butler leads us to [Bassen Ninsk](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md)
- [Bassen Ninsk](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md) wears the same clothes as everyone else but it's super clean and well kept, balding haircut, big gash on his forehead
- [Bassen Ninsk](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md) asks us to go to the crypt and clear it out
- [Bassen Ninsk](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md) offers us 500 gold pieces
	- [Zeal](../Players/Zeal.md) counters back with 100 each
	- [Bassen Ninsk](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md) accepts, 800 gold total, provided we find out who did it
- We get 3 minor healing potions total from the leader of the caravan (4 + CON), bonus action
	1. [Colin](../Players/Colin.md)
	2. [Faurgar](../Players/Faurgar.md)
	3. [Egroth](../Players/Egroth.md)

- We enter [The Crypt in East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/The%20Crypt%20in%20East%20Grove.md)
- I used 1/4 level 1 spell slots
- I used 1/3 bardic inspirations
---
date: 2024-08-13
location:
---
# Past Session Recap
- Blah blah 

# This Session
- Minor shopping spree before the break in
- Uh-oh, Earthquake
- Short but intense
- We get led down a sewer by [Colin](../Players/Colin.md)'s "cousin"
- The sewer gate deep in is locked when it shouldn't be
- We find a guy trapped magically glued to a chest
- After we free him he says he was led to believe the [Miners Guild](Miners%20Guild.md) had a ton of gold by a "hooded figure"
- Had been left there for weeks and seen people walk by without helping him
- We go along and find a door that seems much older than anything else in the sewers
- After some insight checks on [Colin](../Players/Colin.md)'s "cousin" we don't distrust him
- [Egroth](../Players/Egroth.md) opens the door manually
- We find a room with 3 holes, 3 artifacts, 3 coffins, 3 murals
- A crow's feet cipher
	- "Three Royal Voices, forgotten from fame
	- All differ in might, but in death they are the same
	- Their visage descends, in the grave they belong
	- Then send them to darkness with the unspoken song"
- 3 walls with murals on them in stained glass
	- left: a noble and fair king, not a tyrant, but not a saint, who died peacefully in his sleep.
	- middle: A warrior queen, a tyrant, who was beautiful and cruel, who was slain in battle.
	- right: a king born into weakness, who tried to drag everyone around him down, who was slain when his people revolted
- [Baloc](../Players/Baloc.md) pushes a button on the piano and finds crows feet which translate into music notes
- We push the statues in the right holes and [Dalia](../Players/Dalia.md) plays the notes on the piano and the door opens
---
date: 2024-08-20
location: "[[Miners Guild|Miners Guild]]"
---
# Past Session Recap
- Blah blah 

# This Session
- Going deeper into the dark (I have darkvision) dungeon 
- We explore dungeon and there's chairs
- [Zeal](../Players/Zeal.md) places a torch on one and it teleports with a pop and we find it on another one
- We keep exploring and speak with the dead to find a passcode to a door
- Eventually find a creature sitting right next to a chair
- Minor illusion pops to get the creature to run run run into a spike pressure plate trap
- Hack at it till it dies while it's stuck
- Zeal uses his axe to destroy the sigil the creature was on
- We find a chest that has a gauntlet in it attached to an undead hand
- We find stairs that go down and go down them more
- We find crow's feet cypher and a crystal tower
- [Dalia](../Players/Dalia.md) hears an unknown voice through the sending stone
- We continue through and hear loud footsteps and then the door bursts open
---
date: 2024-02-01
location: "[[../Notes/Locations/Probably unimportant to me specifically/East Grove|East Grove]]"
---

- starts getting dark (I have darkvision) and it's raining
- [Tigrun](../Players/Tigrun.md) druidcraft's and tells us the rain will probably last all night
- we set up camp and [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) wakes up
- [Cyrus](../Players/Cyrus.md) takes [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md)'s spellbook before he wakes
- [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) has the same accent as [Zeal](../Players/Zeal.md)
- [Tigrun](../Players/Tigrun.md) tries to intimidate [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) but he doesn't really care.
- [Dalia](../Players/Dalia.md) sits with the children and tries to distract them from the interrogation
- [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md), referring to [Faurgar](../Players/Faurgar.md) says "your friend is keeping secrets from you, haha, now I understand" and asks if the [Monster Hunter guild](../Notes/Other/Monster%20Hunter%20guild.md) knew about him
- [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) says he sees how [Faurgar](../Players/Faurgar.md) could sniff out the truth
- the party isn't able to get much out of [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md)
- [Cyrus](../Players/Cyrus.md) offers to chaperone [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md)
- [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) is a commoner from a nearby town, not [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md)
- a benefactor, [Melf](../Notes/NPCs/Melf.md) offered him test subjects and materials and not ask questions and he took the opportunity for money
- [Melf](../Notes/NPCs/Melf.md) tried to keep his identity secret, thinks he might be a shopkeep in [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md)
- [Melf](../Notes/NPCs/Melf.md) may not be his real name
- [Colin](../Players/Colin.md) pulls out some papers from [Gotrek's Magnificent Wares](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/Gotrek's%20Magnificent%20Wares.md) in [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md)
- [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) wants to stand 30 feet away from us and shout the rest of his info
- [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) says [Melf](../Notes/NPCs/Melf.md) asked him to make arcane sigils in various locations 3 to 10 miles around town
- [Cyrus](../Players/Cyrus.md) casts sleep on [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) and with the help of [Tigrun](../Players/Tigrun.md) takes him far away from the party and slits his throat
- [Tigrun](../Players/Tigrun.md) makes a point of not being there for it
- [Cyrus](../Players/Cyrus.md) begins butchering the corpse (no one is here for this)
- [Cyrus](../Players/Cyrus.md) gets back to the group and maintains that he kept everything that he promised
- Yaaaaay long rest
- [Cyrus](../Players/Cyrus.md) flips through the map [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) had sees the arcane sigils at many points all over the continent of [Mahlus](../Notes/Mahlus.md), a lot of 30ft x 30ft circles
- Watch order
	- [Dalia](../Players/Dalia.md) and [Faurgar](../Players/Faurgar.md)
	- [Zeal](../Players/Zeal.md) and [Egroth](../Players/Egroth.md)
- First watch passes uneventfully
- [Zeal](../Players/Zeal.md) hears something on the edge of camp. Goes and looks and sees a rabbit. D: minor illusion rabbit and a bandit grabs him.
- casts command: freeze on one bandit and the other 2 ditch him 
- [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) had a bad dream and [Dalia](../Players/Dalia.md) stays with her
- Bandit's name is [Denthor](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Denthor.md)
- Night passes
- The party introduces themselves
- [Colin](../Players/Colin.md) is student on leave
- [Tigrun](../Players/Tigrun.md) is traveling scholar
- [Faurgar](../Players/Faurgar.md) is a monster hunter
- [Zeal](../Players/Zeal.md) is ex soldier
- [Cyrus](../Players/Cyrus.md) is a servant of the [Fourfold God](../Notes/Other/Fourfold%20God.md)
- [Egroth](../Players/Egroth.md) is a traveler who shakes peoples hands (a politician according to [Colin](../Players/Colin.md))
- getting back to [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md) and people still are not being let in, 2 new guards by the gate
- [Dalia](../Players/Dalia.md) convinces the guards to let them in
- [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) runs straight to [Locke](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Locke.md) and bursts into tears
- [Dalia](../Players/Dalia.md) has 5 gold remaining
- [Locke](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Locke.md) and [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) have been here for 2 weeks, during which [Gotrek's Magnificent Wares](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/Gotrek's%20Magnificent%20Wares.md) has been abandoned
- [Dalia](../Players/Dalia.md) gets a silver locket from [Lexalia](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Lexalia.md)'s mom
- [Gotrek's Magnificent Wares](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/Gotrek's%20Magnificent%20Wares.md) owned by [Gotrek](../Notes/NPCs/Melf.md), a half elf
- We can check in with [The Farmer](../Notes/NPCs/Probably%20Not%20Important%20Anymore/The%20Farmer.md) who [Grimbeard](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Grimbeard.md) said might have work and also guards want wolf pelts
- [The Crypt in East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/The%20Crypt%20in%20East%20Grove.md) is guarded by a ton of guards
- We go to [Gotrek's Magnificent Wares](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/Gotrek's%20Magnificent%20Wares.md) 
- [Cyrus](../Players/Cyrus.md) triggers a fire trap and almost burns down the shop and ruins a book in a false bottom
- The magic from [The Crypt in East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/The%20Crypt%20in%20East%20Grove.md) drowned out the fire trap so [Cyrus](../Players/Cyrus.md) didn't notice it earlier
- Only thing that survived was part of a cipher and [Cyrus](../Players/Cyrus.md) does their best to copy it down
- Guards come by and kick us out and search us
- [Egroth](../Players/Egroth.md) clocks that their looking for a specific thing and asks what it is, they say that they're just looking for contraband
- [Cyrus](../Players/Cyrus.md) just tries to leave and gets shot, falls to the ground, and flies again
- [Tigrun](../Players/Tigrun.md) punches one of guards and time for initiative next session
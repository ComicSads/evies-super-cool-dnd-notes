---
date: 2024-04-30
location:
---
# Past Session Recap
- Infiltrated [The Church of Marileth](The%20Church%20of%20Marileth.md) and found evidence that they're storing and eating people and feeding them to the local clerics. [Argash](Argash.md) has put himself at the top of the church and isn't that faithful. We reunited with [Egroth](../Players/Egroth.md) and went to sneak out but ran into [Lord Argash](Argash.md)

# This Session
- Fight!
- [Argash](Argash.md) leaves lmao
- [Colin](../Players/Colin.md) runs through the people after [Argash](Argash.md) but one of the 4 knights charges after him
- [Colin](../Players/Colin.md) almost dies and runs back and I heal him
- We kill all the guys and run after [Argash](Argash.md) and find him surround by corpses of clerics who have just performed a mass suicide and he starts chanting and the corpses coalesce into a flesh golem
- WE kill the golem
- [Argash](Argash.md) says if he dies his masters will raise him again
	- [Argash](Argash.md) has some amount of gold 
	- wearing an amulet of a praying woman where her legs form into a sharp point
- they start messing with the amulet, which feels like it's pulsing, and it generates light and pure white and it focuses light on the sigil and clears it before it crumbles and the sigil turns gray
- We end the session :o
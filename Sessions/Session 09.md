---
date: 2024-03-12
location:
---
# Past Session Recap
- A [titan](../Notes/Other/titans.md) woke up, the sigils went in 2 directions, so we decided to split up and follow both, we are heading to [Shieldtown](../Notes/Locations/Shieldtown.md)

# This Session
- Woa first session after the split
- [Egroth](../Players/Egroth.md) shakes all the refugees' hands
- We head out before dark
- We make camp and [Colin](../Players/Colin.md) sits with [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md)
- [Dalia](../Players/Dalia.md) plays a small song
- [Dalia](../Players/Dalia.md) and [Egroth](../Players/Egroth.md) take first watch, and they see a shape approaching, it's [Baloc](../Players/Baloc.md)
- [Baloc](../Players/Baloc.md)'s cart is broken further up the road
- [Zeal](../Players/Zeal.md) and [Colin](../Players/Colin.md) take second watch, [Colin](../Players/Colin.md) flips through the necromancer's book and finds out that there definitely is a weakness in the sigils but can't find out what it is
- [Baloc](../Players/Baloc.md) was a member of the [Architects Guild](../Notes/Locations/Architects%20Guild.md) in [Shieldtown](../Notes/Locations/Shieldtown.md)
- Long rest!
- When we walk, we find leftover scouting towers
- We find [Baloc](../Players/Baloc.md)'s cart and there's a wooden box with no wheels, horses, nothing else. It's been ransacked
- [Baloc](../Players/Baloc.md) gives us all 3 coins even though he has basically nothing
- [Colin](../Players/Colin.md) can't discern where the footprints lead exactly
- [Baloc](../Players/Baloc.md) sends his bird out and about to try and find the bandits and he does
- We go there and find some men who are definitely probably not bandit's
- After trying to talk with them they decide to attack us
- They say they're on official guild business
- Combat :o
- [Baloc](../Players/Baloc.md) buries the bandit leader alive and [Dalia](../Players/Dalia.md) befriends the wolf
- We find a badge belong to the [Miners Guild](Miners%20Guild.md) on the bandit leader
- We find a letter addressed to Grim Bursack talking about how they must stop the darkness that started at [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md) and might be heading to [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md) next, signed A.
- As we head back to the path we get a little lost and eventually find a small road and a farmstead, which is quiet which is unusual at this time of day
- We walk past and see a corpse on the doorstep, with big bite marks in it, couple of days old
- Whatever killed the corpse didn't try to eat it, just bit it and left
- Another corpse by the door, with similar bite marks
- Third corpse in the middle of the room, 2 bed's
- On the bed to the right there is a 4th corpse
- The one on the bed has no bite marks but all other ones do. It's thumbs have been twisted backwards.
- The one on the bed look's pristine and the rest look like they've been dead for a few days
- [Zeal](../Players/Zeal.md) enters the room and instantly feels like he is being watched, there is no second floor or basement.
- In the kitchen there is a 5th corpse with no bite marks
- [Zeal](../Players/Zeal.md) crouches down to the corpse on the kitchen and the corpse's torso opens and snaps at him
- D: Mimics
- :D dead mimics
---
date: 2024-04-09
location: "[[../Notes/Locations/Shieldtown|Shieldtown]]"
---
# Past Session Recap
- Got job with shady [Miners Guild](Miners%20Guild.md). Got some bread, met the alchemist at [The Alchemists Retreat](The%20Alchemists%20Retreat.md), where [Baloc](../Players/Baloc.md) warned the shopkeep, his friend, to leave the city.

# This Session
- We go to the tavern and start planning
- We go get the body of the paladin back for our plan
- We go out of [Shieldtown](../Notes/Locations/Shieldtown.md) to [The Church of Marileth](The%20Church%20of%20Marileth.md), a cathedral comes up on the horizon
- [Colin](../Players/Colin.md) ties a false knot around [Egroth](../Players/Egroth.md)'s wrist
- We go in and brother magus is the name of the dead guy
- Go in to talk to lord [Argash](Argash.md), while [Colin](../Players/Colin.md) sneaks in through the cellar
- [Zeal](../Players/Zeal.md) goes to the kitchen
- [Colin](../Players/Colin.md) found a room with preserved torso's with no hands, feet, or heads
- [Zeal](../Players/Zeal.md) finds out the church is serving "pork" tonight
- Everyone but [Egroth](../Players/Egroth.md) reunites and puts together that everyone who eats the food will be turned into undead
- lord [Argash](Argash.md) reveals to have mind controlled almost all of the knights
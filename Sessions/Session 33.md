---
date: 2025-02-04
location: "[[The Grand Academy]]"
---
# Past Session Recap
- We went into a chamber and killed a lich

# This Session
- Chamber rumbles a little
- [Zeal](Zeal.md) finds a thing and tosses it [Baloc](Baloc.md)
- [Baloc](Baloc.md) does some magic on the sigil
- We enter a room and find a bunch of bodies
- We find some living people in the pile of dead bodies
- We enter another room we find a box
- Inside the box is a heart
- We pick up all the people and [Zeal](Zeal.md) picks up a box
- We run outside and see the [titan's](titans.md) hand has moved and looks like it's trying to grab the school but currently isn't moving
- We start packing up and go to the [Miners Guild](Miners%20Guild.md) and it's been ruined cause it's built into the skull
- Have 15 zombies shambling towards us
---
date: 2024-05-14
location:
---
# Past Session Recap
- Got 100 gp each

# This Session
- [Baloc](../Players/Baloc.md) opens a chest with a key from [Argash](Argash.md) and finds 
	- a bunch of books 
		- mostly prayer books
		- necromancer tomes
		- spell scrolls
			- blur
			- misty step
	- a letter covers in crows feet
- And a second chest
	- filled with cloths mostly
	- +1 short sword wrapped in silk that can cast light at will
	- amulet of speak with dead
- We break a window and jump out and use feather fall then just start running
- The second chest will wrap whatever you put in it in silken cloth, the cloth will get deleted later
- [Argash](Argash.md) mentions he's a pawn and if he dies he'll just come back
	- Gets messages from corpses that finds him
	- around 2 weeks ago was his most recent one
	- [Ardweld](Ardweld.md) is a location
- Druid co-conspirator
	- In a druids grove in the forest
	- working there for a long time
- We get a piece of paper to decode messages written in crows feet from [Argash](Argash.md)
- Plan is for titans to rise and cause [Mahlus](../Notes/Mahlus.md) to burn
- [Argash](Argash.md) has nothing against [Mahlus](../Notes/Mahlus.md) but he wants to ascend from the tyranny of the gods
- During [Dalia](../Players/Dalia.md)'s watch a wisp comes up to the party, [Dalia](../Players/Dalia.md) wakes [Baloc](../Players/Baloc.md) and the wisp named [Vi](Vi.md) tells them about a grove that has been rotted
- [Baloc](../Players/Baloc.md) decodes the letter
	- Signed by M [Melf](../Notes/NPCs/Melf.md)?
	- Says the guild's don't know anything
	- Evil plan running well, is complete, once the church sigil is ready they'll get started
- We kill a rat to deliver a false letter to [Melf](../Notes/NPCs/Melf.md)
	- [Vi](Vi.md) is very unhappy with this
- [Zeal](../Players/Zeal.md) and [Egroth](../Players/Egroth.md) tell all the relevant to the [Miners Guild](Miners%20Guild.md) 
	- They get 75G each for the party
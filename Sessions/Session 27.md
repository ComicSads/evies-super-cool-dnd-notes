---
date: 2024-10-08
location:
---
# This Session
- The hatch we climbed through last game lead us into the [Miners Guild](Miners%20Guild.md)
- 2 soldiers start questioning us so [Zeal](../Players/Zeal.md) knocks them unconcious
- [Egroth](../Players/Egroth.md) fails to open a door D:
- [[Colin]] picks a lock and finds the leader [Mr. Bavin](Mr.%20Bavin.md) of the [Miners Guild](Miners%20Guild.md) and a cloaked guy
- Cloaked guy is [Draelon](Draelon.md) an elf wizard from the society of mages from the city of [Clandestine](../Notes/Locations/Clandestine.md)
- We lead them through the closet and the hatch
- The gang gets in a bit of an argument with [Draelon](Draelon.md) about whether or not the knowledge of summoning titans should be allowed to exist, [Draelon](Draelon.md) thinks it will make it easier to defeat them
- We get food (confirmed not human by an invisible [Dalia](../Players/Dalia.md)) and take a bath and get ready for bed
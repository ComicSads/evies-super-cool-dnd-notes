---
date: 2024-12-10
location: "[[The Grand Academy]]"
---
- [Baloc](../Players/Baloc.md) finds a undamaged wooden lute and gives it to [Dalia](../Players/Dalia.md), it let's her summon elemental creatures
- We go to [The Grand Academy](The%20Grand%20Academy.md) which has the final cult outpost in [Shieldtown](../Notes/Locations/Shieldtown.md)
	- [Colin](../Players/Colin.md) and [Baloc](../Players/Baloc.md) have been here before but the party only knows about [Baloc](../Players/Baloc.md)
- Sneaking into the headmasters office reveals many people went missing and the headmaster [Stradavarius Gael](Stradavarius%20Gael.md) tried to ask for help on the [Miners Guild](Miners%20Guild.md) but was denied.
- [Stradavarius Gael](Stradavarius%20Gael.md) narrowed it down to 3 people who could be behind it
	- [Barnaby Hagglesworth](Barnaby%20Hagglesworth.md), Divination Magic Teacher
	- [Alerie Nathala](Alerie%20Nathala.md), Evocation Magic Teacher
	- [Stradavarius Gael](Stradavarius%20Gael.md), Headmaster.
- [Egroth](../Players/Egroth.md) distracts [Stradavarius Gael](Stradavarius%20Gael.md)
- We go to [Alerie Nathala](Alerie%20Nathala.md)'s office and in her journal she thinks she's getting fired because of how [Stradavarius Gael](Stradavarius%20Gael.md) is talking to her
- [Barnaby Hagglesworth](Barnaby%20Hagglesworth.md) knows we're coming and is waiting for us in his office
- Says he has noticed people missing but hasn't told anyone
- he suggests we go to the storage basement to look for a mirror
- We plan to have [Colin](../Players/Colin.md) stay behind and see if [Barnaby Hagglesworth](Barnaby%20Hagglesworth.md) tried to get a message out
---
date: 2024-03-26
location: "[[../../Locations/Shieldtown|Shieldtown]]"
---
# Past Session Recap
- Traveled further on the road, met [Grimgott](../Notes/NPCs/Grimgott.md) who sold "useful" magic items. We chilled in a tavern and there was a paladin asking [the bartender](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jerry.md) for money and [Zeal](../Players/Zeal.md) killed him in an honor duel. [Zeal](../Players/Zeal.md) and [Baloc](../Players/Baloc.md) went to [Shieldtown](../Notes/Locations/Shieldtown.md) to report it and told them not to track down the body and the party would do it themselves. They came back and went to bed 

# This Session
- [Colin](../Players/Colin.md) asks [Baloc](../Players/Baloc.md) what he thinks the smaller seals around [Shieldtown](../Notes/Locations/Shieldtown.md) are on the map. They surround the mass resurrection spell but seem to just be arcane focuses to empower the main spell. Destroying them will likely slow down the main seal
- We travel to [Shieldtown](../Notes/Locations/Shieldtown.md) proper
- [Dalia](../Players/Dalia.md) asks [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) if she knows where her family is. They move from place to place pretty often so she doesn't know.
- We meet a random woman on the way
- [Baloc](../Players/Baloc.md) gets off and the woman grabs his hand and it feels like bramble and she starts laughing
- Initiative!
- We beat up the treants 
- [Egroth](../Players/Egroth.md) drinks mud
- [Baloc](../Players/Baloc.md) makes the mud look like water and [Egroth](../Players/Egroth.md) drinks it again
- We finally make it to [Shieldtown](../Notes/Locations/Shieldtown.md)
- We get let in without issue
- It's dark under the shield
- We see a skull which is the entrance to the [Miners Guild](Miners%20Guild.md)
- We see a tavern called [The Weathered Whisker](../Notes/Locations/The%20Weathered%20Whisker.md)
- [Baloc](../Players/Baloc.md) and [Colin](../Players/Colin.md) go to the [Shieldtown Library](../Notes/Locations/Shieldtown%20Library.md)
- Dalia goes to the market to find someone who might be able to find [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) a forever home
- [Zeal](../Players/Zeal.md) and [Egroth](../Players/Egroth.md) go to the [Miners Guild](Miners%20Guild.md)
- [Baloc](../Players/Baloc.md) and [Colin](../Players/Colin.md) keep getting distracted and wikipedia-rabbit-holed and don't find info at first
- The librarian comes up and finds them another book
- [Colin](../Players/Colin.md) finds that a conduit is most often used in rituals, and every conduit always needs an anti-conduit to break the ritual
	- The conduit doesn't need to be an object but does need to be in the physical plane, can be a marking or a person
- [Dalia](../Players/Dalia.md) and [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) go to the market and meet [Lyra](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Lyra.md), an Orc woman who knew [Locke](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Locke.md) before Tifa's birth
- [Lyra](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Lyra.md) takes [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) and [Betsy](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Betsy.md) into her care and [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) and [Dalia](../Players/Dalia.md) say their goodbye's
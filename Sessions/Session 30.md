---
date: 2024-11-19
location: "[[Shieldtown]]"
---
# Past Session Recap
- Got the details of the 3 members of [The Church of Marileth](The%20Church%20of%20Marileth.md) who went to see [Mr. Bavin](Mr.%20Bavin.md) supposedly

# This Session
- Get to [The Weathered Whisker](The%20Weathered%20Whisker.md) looking for [Wizz Spitfizzle](Wizz%20Spitfizzle.md) [Armin Dilth](Armin%20Dilth.md) and [Laeva Farogh](Laeva%20Farogh.md)
- [Dalia](Dalia.md) starts performing as a distraction while [Zeal](Zeal.md) finds the table
- [Dalia](Dalia.md) attempts to charm the people at the table but no one knows
- [Colin](Colin.md) gets spotted trying to pickpocket them
- [Zeal](Zeal.md) goes up and with [Colin](Colin.md) goes out into the alleyway with the 3 bad guys
- [Dalia](Dalia.md) tries to talk with them but doesn't get anything useful
- Bad guys lead [Colin](Colin.md) and [Zeal](Zeal.md)
- [Egroth](Egroth.md) realizes they're stalling, probably for reinforcements
- [Dalia](Dalia.md) and [Baloc](Baloc.md) get in hiding spots, disguised as an old man and a roof top respectively
- [Colin](Colin.md) canonically throws [Dalia](Dalia.md) all of his money
- [Armin Dilth](Armin%20Dilth.md) resurrects his wife and another ghost
- [Baloc](Baloc.md) fireballs a surrendering [Wizz Spitfizzle](Wizz%20Spitfizzle.md) and [Laeva Farogh](Laeva%20Farogh.md)
- [Colin](Colin.md) slits [Armin Dilth's](Armin%20Dilth.md) throat and takes the amulet
- The amulet controls the banshee
- The banshee tells us the cult only has one more outpost in this city and that's where the mirror was taken
- They tell us about [Zuul](Zuul.md)
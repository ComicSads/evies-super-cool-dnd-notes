---
date: 2024-09-10
location:
---
# This Session
- Reach the top of the spire
- We find a bunch of hospital-style gurneys
- Follow down a hallway and [Baloc](../Players/Baloc.md) starts burning corpses in prison cells and [Egroth](../Players/Egroth.md) and [Dalia](../Players/Dalia.md) find a treasure chest
	- 6 Healing potions
	- Potion of arcane cultivation
	- Parchment that just says 
- We go south and find a desk
- Go east and find a horrifying flesh monster which we fight
- We go past it an there's a room with a mirror but when [Baloc](../Players/Baloc.md) gets closer the reflection fades to show a completely different room
- We back out to deal with it later and find an organ with a pile of blood and statues of people climbing over themselves
- Find a letter on the gurney
	- From a dead guy to his wife
- Eventually manage to solve the gem puzzle and get a mirror open and see a bone effigy
- The bone effigy starts walking towards us and we walk through the mirror
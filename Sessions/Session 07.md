---
date: 2024-02-29
location: "[[../Notes/Locations/Probably unimportant to me specifically/East Grove]]"
---
# Past Session Recap
- Legit just combat. [Tigrun](../Players/Tigrun.md) and [Zeal](../Players/Zeal.md) got eaten by a flesh golem and we had to save them.

# This Session
- Yay my character sheet is (mostly) in roll20 now so I don't have to keep track of stats here.
- There's a door with eldritch whispering behind it, [Faurgar](../Players/Faurgar.md) peaks through the keyhole and sees an undead ogre and a floating red thing that's whispering. In front of the red thing there's a red sigil which we saw before in the necromancer's book
- [Faurgar](../Players/Faurgar.md) asks [Cyrus](../Players/Cyrus.md) if there's anything like the sigil he sees in the necromancer's book
- [Evie](../Players/Dalia.md) needs to fix her initiative to account for jack of all trades
- We find a necromancer who is probably [Melf](../Notes/NPCs/Melf.md) in the room with 2 undead ogres and we fight for a bit but then the necromancer bamf's out of there after saying "You're too late"
- [Dalia](../Players/Dalia.md) fucking dies (is at 0 hp) then [Nola](../Players/Nola.md) brings her back
- We clean up the rest of the undead and the ground starts shaking
- [Cyrus](../Players/Cyrus.md) copies down a sigil and [Tigrun](../Players/Tigrun.md) picks up a chest
- We run out of [The Crypt in East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/The%20Crypt%20in%20East%20Grove.md) and see the shaking isn't just limited to the crypt, the whole town is shaking, the guards have left, the people are running around, buildings are crumbling
- No one looks like they have any idea what's going on
- We go to [The Magistrate](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md)'s office and he's fully packed up
- There is a huge sulfur smell
- [Tigrun](../Players/Tigrun.md) grabs [Bassen Ninsk](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md) and demands their pay before deciding to let him go
- [Cyrus](../Players/Cyrus.md) grabs 5 books from [The Magistrate](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md)'s office
- The ground is shaking and we see the rib cage of the [titan](../Notes/Other/titans.md) is glowing green
- We start running out of town, [Faurgar](../Players/Faurgar.md) stops to help get injured people out of the town
- We see [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) crying in front of [Locke](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Locke.md)'s corpse as he starts to reanimate, [Zeal](../Players/Zeal.md), [Colin](../Players/Colin.md), and [Dalia](../Players/Dalia.md) all run to get [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) out
- [Tigrun](../Players/Tigrun.md) throws out the chest and picks up 2 people
- We all get out and see the road rise up as a knee shoots out of the ground
- The entire [titan](../Notes/Other/titans.md) gets up now, we see sigils all over the body as it strides off
- Yay session over
- Everyone levels up to 4
- [Colin](../Players/Colin.md) is laughing and saying how it's amazing since necromancy is disallowed in the academy

Splitting the party :c

| CAX                           | Sara                         |
| ----------------------------- | ---------------------------- |
| [Sara](../Players/Tigrun.md)  | [CAX](../Players/Baloc.md)   |
| [Thorn](../Players/Cyrus.md)  | [Jake](../Players/Egroth.md) |
| [Bun](../Players/Nola.md)     | [Smisu](../Players/Colin.md) |
| [John](../Players/Faurgar.md) | [Evie](../Players/Dalia.md)  |
| Daniella                      | [Rophy](../Players/Zeal.md)  |
- Everyone shows up on Thursday as normal
- Sara's group Tuesday 7:00 EST
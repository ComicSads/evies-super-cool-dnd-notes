---
date: 2024-02-08
location: "[[../Notes/Locations/Probably unimportant to me specifically/East Grove|East Grove]]"
---
- Minor retcon to the point we're walking outside [Gotrek's Magnificent Wares](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/Gotrek's%20Magnificent%20Wares.md)
- also [Evie](../Players/Dalia.md) and [Sara](../Players/Tigrun.md) get 1 dm inspiration each
- guards shake us down
	- take a crowbar from [Colin](../Players/Colin.md)
	- one looks at the necromancer book [Cyrus](../Players/Cyrus.md) got from [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md) and looks surprised
	- [Cyrus](../Players/Cyrus.md) bribes them to leave with 5 gold
	- we get the crowbar back!!1
- [Grimbeard](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Grimbeard.md) gives us 5 rooms for free cause we're so cool
- on the map in [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md)'s book there's a bunch of sigils all over [Mahlus](../Notes/Mahlus.md), one is in [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md) 
- Due to bad math we all get money from the pelts back + 10 extra (to be divvied up)
- [Tigrun](../Players/Tigrun.md) stays in inn room with some contraband
- Scoping out the wall
	- [Dalia](../Players/Dalia.md)
	- [Cyrus](../Players/Cyrus.md)
	- [Colin](../Players/Colin.md)
	- [Egroth](../Players/Egroth.md)
- Checking out the crypt
	- [Faurgar](../Players/Faurgar.md)
	- [Zeal](../Players/Zeal.md)
	- [Nola](../Players/Nola.md)
	
- Scoping the town we find that it's not a very large town, tavern very close to the entrance, north from entrance is magistrates office, barracks on the left
- Just stroll around looking for weak points in the wall
- find a shimmiable spot in the East that gets used fairly often, but not big enough for [Tigrun](../Players/Tigrun.md) to go under, but he might be able to jump?
- Checking out of the crypt stuff happens
- [Nola](../Players/Nola.md) turns into a rat and sneaks in and sees stuff but I wasn't really paying attention cause my character wasn't there
	- A bunch of undead corpses just chilling
	
- we try to come up with a plan to get in the crypt
	- [Dalia](../Players/Dalia.md) will disguise self into the sergeant and tell the guards to leave for some reason
	- if this does not work [Cyrus](../Players/Cyrus.md) will burn down a house
- We get description of sergeant from [Grimbeard](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Grimbeard.md)
- Guards stop us as we leave the tavern
- Magistrate would like to meet with us
- [Cyrus](../Players/Cyrus.md)' familiar comes back
---
date: 2024-10-29
location: "[[Miners Guild|Miners Guild]]"
---
# Past Session Recap
- Go through the dungeon into the [Miners Guild](Miners%20Guild.md) and explain the sitch to [Mr. Bavin](Mr.%20Bavin.md) and [Draelon](Draelon.md) who's also

# This Session
- The cafeteria man says they have a squid creature that [Baloc](../Players/Baloc.md) realized is a mind flayer
	- Says a bounty hunter group found a colony up North searching for something else
- Mindflayer tentacles very chewy and make [Baloc](../Players/Baloc.md) feel a lot more focused and gets a +2 to int and wis
- The rest of us get -1 wis and int based rolls which [Zeal](../Players/Zeal.md) instantly cures us with paladin powers
- Egroth eats more and gets a big int bonus
- We go to bed and [Baloc](../Players/Baloc.md) and [Draelon](Draelon.md) stay up and work on the cipher
- [Baloc](../Players/Baloc.md) finds out the evil necromancers basically know who we are and are sending hitmen and really really want the urn
- We go down into the evil room to burn all the books and find out the mirror is missing
- We leave and find the door barred and break it down and talk to guards to try to chase down a sign member of [The Church of Marileth](The%20Church%20of%20Marileth.md)
- Find a (probably) different member who talks about a meeting with [The Church of Marileth](The%20Church%20of%20Marileth.md) and [Mr. Bavin](Mr.%20Bavin.md)
- Tell the front guard to stop any other members of the church they see
- Wake up [Draelon](Draelon.md) and tell him we went downstairs and saw the books burning
- Talk to [Mr. Bavin](Mr.%20Bavin.md) and question him and he introduces us to his secretary [Sally](Sally.md)
- [Sally](Sally.md) shows us the form the members of [The Church of Marileth](The%20Church%20of%20Marileth.md) gave her and the stamp is counterfeit after searching
- He offers us 600 coins, half now half after investigating [The Church of Marileth](The%20Church%20of%20Marileth.md) (again)
	- 300/5 = 60 ea
- Stop by the market
- [Grimgott](../Notes/NPCs/Grimgott.md)'s market
- [Egroth](../Players/Egroth.md) spends 100G on a sorcerer thumb
- [Dalia](../Players/Dalia.md) spends 50G on a magically hot frying pan and a murky black snow globe (snow globe was free cause the frying pan is a fire hazard)
- [Baloc](../Players/Baloc.md) spends 75G on a magic staff

---
date: 2024-06-18
location: "[[Ardweld]]"
---
# Past Session Recap
- Blah blah 

# This Session
- [Baloc](../Players/Baloc.md) wakes everyone up before running upstairs
- We find a sending stone following the trail of the creature
- We find another unrelated one in the druid keep by [Aurela](Aurela.md)'s bed
- Speaking into it [Baloc](../Players/Baloc.md) hears [Barov Thunderpunch](Barov%20Thunderpunch.md) respond "Thief" in a gravelly voice
- We go after [Barov Thunderpunch](Barov%20Thunderpunch.md) and find his potion shop and it's empty\
- We find a secret door that goes down into stairs to a pyre
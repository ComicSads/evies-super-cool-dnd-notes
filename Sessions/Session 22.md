---
date: 2024-07-30
location:
---
# Past Session Recap

- Blah blah

# This Session

- [Baloc](../Players/Baloc.md) and [Colin](../Players/Colin.md) go to the library and [Baloc](../Players/Baloc.md) gets a magical headache
- The urn is probably a soul conduit
- [Baloc](../Players/Baloc.md) gets very sleepy very quickly
- [Dalia](../Players/Dalia.md) buys preserved meats from [Gnoril](Gnoril)
- [Egroth](../Players/Egroth.md) and [Zeal](../Players/Zeal.md) sneak into the [Miners Guild](Miners%20Guild.md), find a book that talks about how they mined too deep
- Go to their unrelated meeting at the [Miners Guild](Miners%20Guild.md), get given a list of potential culty stuff
- Take the urn to the church and the guy says we have to go north to the town of [Clandestine](../Notes/Locations/Clandestine.md) where the head church is
- We go to [Colin](../Players/Colin.md)'s meetup bread place and he sets up a meeting later in the day
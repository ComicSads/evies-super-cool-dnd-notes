---
date: 2024-07-16
location:
---
# Past Session Recap

- Blah blah 

# This Session
- We decided not to blow up the crystal for now
- 20 gold ea
- [Baloc](../Players/Baloc.md) reads a scroll and also talks to a bone effigy
- Bone effigy's named [Theodore](Theodore.md) master's name is [Jay](Jay.md)
- we steal some shit including an urn that [Dalia](../Players/Dalia.md) tries to steal from [Baloc](../Players/Baloc.md)
- we clear [Dalia's](../Players/Dalia.md) curse
- [Zeal](../Players/Zeal.md) and [Dalia](../Players/Dalia.md) play rummy with a bunch of a strangers in a tavern while [Baloc](../Players/Baloc.md) studies shit
- [Colin](../Players/Colin.md) and [Baloc](../Players/Baloc.md) will go to the library and research while everyone gets supplied up
- [Egroth](../Players/Egroth.md) and [Zeal](../Players/Zeal.md) will use the staff to talk to the [Miners Guild](Miners%20Guild.md) boss while everyone looks for where the portal was and who it could belong to
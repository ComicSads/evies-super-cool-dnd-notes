---
date: 2024-09-23
location:
---
# Past Session Recap
- So I accidentally sent my notes for this session to the shadow realm while doing things on my computer
# This Session
- We fight [Theodore](Theodore.md) our friend
- Find the corpse of Lord Argash and question him some, seems like he was broken out of prison
- Wipe away a fresh blood sigil
- Find a ceiling hatch and go into it

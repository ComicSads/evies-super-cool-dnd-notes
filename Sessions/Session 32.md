---
date: 2025-01-14
location: "[[The Grand Academy]]"
---
# Past Session Recap
- Blah blah 

# This Session
- Psych we're taking [Colin](Colin.md) and [Barnaby Hagglesworth](Barnaby%20Hagglesworth.md) down to the storage basement with us
- [Baloc](Baloc.md) tells our plan to [Stradavarius Gael](Stradavarius%20Gael.md) and he helps us
- We go to the storage basement and [Stradavarius Gael](Stradavarius%20Gael.md) casts locate object targeting mirrors and it goes through the wall
- [Egroth](Egroth.md) runs through the wall
- We find a magic mouth which is serving as a lock for the door
- Inspection reveals it could cast silence
- We tie up [Barnaby Hagglesworth](Barnaby%20Hagglesworth.md) and make him crawl and that solves the puzzle
- In the next room we wake up a [The Lich](The%20Lich.md)
- [Dalia](Dalia.md) and [Zeal](Zeal.md) get dominated and the [The Lich](The%20Lich.md) tries to leave
- We beat up the [The Lich](The%20Lich.md)
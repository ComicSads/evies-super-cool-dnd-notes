---
date: 2024-06-04
location:
---
# Past Session Recap
- We beat up some trees and stopped the druid ritual

# This Session
- We walk down a spiraling walkway
- [Baloc](../Players/Baloc.md) eventually realizes we can mold earth and dig straight down
- Eventually we realize we can drop on the necromancer chamber
- [Dalia](../Players/Dalia.md) and [Egroth](../Players/Egroth.md) drop from the ceiling while [Baloc](../Players/Baloc.md) [Zeal](../Players/Zeal.md) and [Colin](../Players/Colin.md) go in from the front door
- There's a purple orb in the center that's dissolving meat
- Beat up the necromancer non lethally
- [Aurela](Aurela.md) was in the crystal
- We go to bed but wake up in a void minus [Zeal](../Players/Zeal.md) on watch
- [Aurela](Aurela.md) says "The heart is the key, the heart is what you seek"
- The skull [Baloc](../Players/Baloc.md) crushed with an amulet came back
- [Baloc](../Players/Baloc.md) realized that this means the creature we killed above ground might be regenerated
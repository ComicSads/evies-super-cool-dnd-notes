---
date: 2024-04-02
location: "[[../Notes/Locations/Shieldtown|Shieldtown]]"
---
# Past Session Recap
- We made it to [Shieldtown](../Notes/Locations/Shieldtown.md) and [Baloc](../Players/Baloc.md) and [Colin](../Players/Colin.md) went book hunting, [Dalia](../Players/Dalia.md) found a forever home for [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md). [Egroth](../Players/Egroth.md) and [Zeal](../Players/Zeal.md) went to the [Miners Guild](Miners%20Guild.md) and now we see what happens...

# This Session
- The Guard has orders to only let in people with specific business related to East Grove 
- They get brought to the backroom with a guy who asks if it's true
- We find out [Gotrek](../Notes/NPCs/Melf.md) was likely a real person before [Melf](../Notes/NPCs/Melf.md) assumed his identity
- [Miners Guild](Miners%20Guild.md) offers us a job (50G ea) to investigate [The Church of Marileth](The%20Church%20of%20Marileth.md), the church of the paladin [Zeal](../Players/Zeal.md) killed in an honor duel
- Code word to get back in [Miners Guild](Miners%20Guild.md) is arch-ee-pelago
- At [The Weathered Whisker](../Notes/Locations/The%20Weathered%20Whisker.md) the party meets up. Nice old woman as the bartender
- A strong purge of life magic could counteract the seals so it may be worth checking out [The Church of Marileth](The%20Church%20of%20Marileth.md)
- [The Church of Marileth](The%20Church%20of%20Marileth.md)'s tenants are not fluid and very lawful and would chop off a child's arm for stealing bread. Sin no matter how big or small should be punished, and everything can be pure
- [The Church of Marileth](The%20Church%20of%20Marileth.md) has been chill for a while but started causing more issues more recently
- [Baloc](../Players/Baloc.md) does not trust the [Miners Guild](Miners%20Guild.md) even a little and thinks they may just be trying to cover their own ass in [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md)
- Game plan is [Colin](../Players/Colin.md) looks around [The Church of Marileth](The%20Church%20of%20Marileth.md) at night to find access points and the rest of us talk through the front door
- Arch-ee-pelago is a botanist who named a lot of flora and fauna. There's a healing herb called the pelagosa named after him
- [Baloc](../Players/Baloc.md) notices a lot of previously easily accessible stuff that would have been in museums and librarys are now locked in this private collection
- We get back to the dwarf boss now with an accountant
- We agree to 85G ea, for information specifically on [The Church of Marileth](The%20Church%20of%20Marileth.md)
- Agree to a contract that [Colin](../Players/Colin.md) gives a thumbs up on but states punishment to be delivered by the miners guild if we don't performing the task
- The dwarf boss asks to speak with [Zeal](../Players/Zeal.md) alone
- Says that he recognizes [Zeal](../Players/Zeal.md) worked for another guild and reminds him he's working for the [Miners Guild](Miners%20Guild.md)
- [Zeal](../Players/Zeal.md) just says the boss recognized that he fought in previous wars
- We go to the market ready to find useful items
- We get distracted by bread
- Gadzooks! The bakery is a secret thief hideout [Colin](../Players/Colin.md) recognizes
- Baker is named [Magdalena](Magdalena.md) and uses mostly storebought bread. She was once the greatest thief in shieldtown
- She recommends [The Alchemists Retreat](The%20Alchemists%20Retreat.md) for potions
- We head there and find a guy [Baloc](../Players/Baloc.md) recognizes [Barov Thunderpunch](Barov%20Thunderpunch.md) as the shopkeep
	- Some 3 hooded figures came by 2 days and smelt of sulfur and bought some components
	- [Baloc](../Players/Baloc.md) writes down all the components they got
	- [Baloc](../Players/Baloc.md) tells [Barov Thunderpunch](Barov%20Thunderpunch.md) to get out of the town for at least a week
	- We buy 10 healing potions, 2G ea
		- everyone gets 2 ea
	- And a potion of invisibility, 5G, for [Egroth](../Players/Egroth.md)
---
date: 2024-02-22
location: "[[../Notes/Locations/Probably unimportant to me specifically/East Grove]]"
---
# Past Session Recap
- We talked about how to break into [The Crypt in East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/The%20Crypt%20in%20East%20Grove.md) but [The Magistrate](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md)'s men take us to talk to him and he asks us to clear out The Crypt. Combat has happened and more will.

# This Session
- I used 2/4 level 1 spell slots
- I used 1/2 level 2 spell slots
- I used 2/3 bardic inspirations
- GM inspiration lasts only for the session apparently so everyone loses theirs
- We set up a killbox where I cast Cloud of Daggers doing $9+11+12+12+11+6+7+8+11+9+11=107$ damage
- All of the sudden, a sigil in the other room activates and [Tigrun](../Players/Tigrun.md) and [Zeal](../Players/Zeal.md) get sucked into a mass of zombies which starts to turn into a flesh golem.
- [Faurgar](../Players/Faurgar.md) turns into his werewolf form which everyone freaks out over after the fight
- We find a lot of stuff in a chest
	- A book on the [Fourfold God](../Notes/Other/Fourfold%20God.md)
	- Coin purse with 400 gold
	- A book called: Ancient musings on [the titans](../Notes/Other/titans.md)
	- A scroll of burning hands
	- +1 Axe of sharpness
- Tall of party gold to be split: 410 gold
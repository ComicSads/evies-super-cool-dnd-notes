---
date: 2024-11-12
location: "[[Shieldtown]]"
---
# Past Session Recap
- Blah blah 

# This Session
- [Colin](../Players/Colin.md) writes a letter to the Thieves Guild off screen
- [Baloc](../Players/Baloc.md) identifies everything
- [Alavia](Alavia.md), the temporary high bishop accepts us under the guise of being tax inspectors
	- Mentions she sent an envoy to the [Miners Guild](Miners%20Guild.md)
- The envoys left [The Church of Marileth](The%20Church%20of%20Marileth.md) and can't be found, [Alavia](Alavia.md) gives us their names
	- [Armin Dilth](Armin%20Dilth.md) - tiefling man
	- [Laeva Farogh](Laeva%20Farogh.md) - half elf drow
	- [Wizz Spitfizzle](Wizz%20Spitfizzle.md) - gnome
- In [Laeva's](Laeva%20Farogh.md) room [Colin](../Players/Colin.md) finds a chicken scratch
- on the way back [Baloc](../Players/Baloc.md) translates it and finds it has instructions to meet mr. bavin then regroup in a tavern, Whisker.(don't know what whisker means here)
	- Chicken scratch is written very poorly
- We realize whisker probably means [The Weathered Whisker](../Notes/Locations/The%20Weathered%20Whisker.md)
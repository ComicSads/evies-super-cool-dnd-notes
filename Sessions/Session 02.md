---
date: 2024-01-25
location: "[[../Notes/Locations/Probably unimportant to me specifically/East Grove|East Grove]]"
---

- Heading out of [East Grove](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/East%20Grove.md), starting to get late
- going to the spot we were directed to by the parents
- general bounty for pelts of predator animals that make the roads dangerous
- [Faurgar](../Players/Faurgar.md) scans the area and finds some promising footprints
- follow them and lose the prints around where we find some toys including an injured doll and burn marks
- [Faurgar](../Players/Faurgar.md) smells the toy and a decent chunk of the party sees this, only [Dalia](../Players/Dalia.md) mentions how everyone is trying to get the children back safe and sound
- [Faurgar](../Players/Faurgar.md) starts heading in a direction and pretends to know where to go because of a stick
- [Tigrun](../Players/Tigrun.md) druid's a flower on the stick
- party finds a cave
- [Egroth](../Players/Egroth.md) finds a very very old blood trail leading into the cave, not even sure it is blood at first
- cave is dark (i have dark vision) but [Tigrun](../Players/Tigrun.md) takes a stick and druid's it into a fire
- we survive a stealth check and see some wolves, a campfire, and 3 children with their hands and feet tied, surrounded by figures including [Jericho](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Jericho.md)
- smells very bad, like corpses
- [Faurgar](../Players/Faurgar.md) pulls out his crossbow and tries to shoot, party tries to coordinate a bit more, he ends up shooting without a plan anyways
- Initiative!
- 1 bardic inspirations left
- 2 level one spell shots left
- all level two spell slots left
- look in other room
	- see like a mass grave
	- ~~pile~~ mass of an uncountable number of corpses
	- all sewn into one thing
	- it's a creature made of the diseased body of the murdered, aged, and diseased
- we burn it. whatever it is.

- maybe lie about what necromancer was in case [Bassen Ninsk](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Bassen%20Ninsk.md) attempts to be like "ah yes i knew the necromancer was evil that's why we went on lockdown" even if he's just crazy
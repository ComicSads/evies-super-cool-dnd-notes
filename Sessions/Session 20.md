---
date: 2024-07-02
location: "[[../Notes/Locations/Shieldtown|Shieldtown]]"
---
# Past Session Recap
- Blah blah 

# This Session
- Pyre isn't religious but is burning evidence
- [Egroth](../Players/Egroth.md) spots pressure plate and [Baloc](../Players/Baloc.md) triggers it with mage hand
- A pillar pops up crushingly
- we go to the end of the room avoiding pressure plates and we see a mirror
- it asks us to sing an evil necromancer song
- [Dalia](../Players/Dalia.md) does and gets cursed then the mirror turns into a portal
- We go through and find bunk beds, unmagic brooms, and toilets
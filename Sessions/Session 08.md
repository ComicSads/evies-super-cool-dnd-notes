---
date: 2024-03-07
location: "[[../Notes/Locations/Probably unimportant to me specifically/East Grove]]"
---
# Past Session Recap
- We got out of the dungeon and a titan appeared and a lot of people died and we got some out including [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md)

# This Session
- Nearest town is [Shieldtown](../Notes/Locations/Shieldtown.md), a couple miles away, but most of the survivors aren't packed
- [Cyrus](../Players/Cyrus.md) points out someone must have figured out how to raise a [titan](../Notes/Other/titans.md) like a common skeleton
- [Cyrus](../Players/Cyrus.md) looks at the map of sigils and sees that [Shieldtown](../Notes/Locations/Shieldtown.md) has a sigil and so does another nearby town
- There are twelve large x's on the map and a variety of smaller ones that surround the big ones
- The sigils kinda split off in two directions forming an arch-ish shape
- Sara's group decides to go to [Shieldtown](../Notes/Locations/Shieldtown.md)
- CAX's group decides to go to the deserts of [Solus](../Notes/Locations/Probably%20unimportant%20to%20me%20specifically/Solus.md)
- [Zeal](../Players/Zeal.md) asks the NPCs if they're planning to go to [Shieldtown](../Notes/Locations/Shieldtown.md) they can go with Sara's group to be protected on the way there
- [Tigrun](../Players/Tigrun.md) grows a flower (pansy) for everyone in Sara's group
- [Cyrus](../Players/Cyrus.md) hands a scrawled map to [Dalia](../Players/Dalia.md) and the necromancer's spellbook to [Colin](../Players/Colin.md)
- Everyone gets 51 gold (split from the 410 gold), the 2 extra get's deleted
- [Tifa](../Notes/NPCs/Probably%20Not%20Important%20Anymore/Tifa.md) asks if she can go with [Tigrun](../Players/Tigrun.md), but he says it wont be safe and she starts crying as the other party goes with her
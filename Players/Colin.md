---
aliases:
  - Smisu
Class: Rogue
---
A thin Goliath who mostly sticks to himself and seems annoyed by everyone around him.
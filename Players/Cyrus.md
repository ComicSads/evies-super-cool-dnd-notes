---
aliases:
  - Thorn
Class: Wizard
pronouns: he/they
---
Dragonborn birdperson who is a cleric to some diety I don't know. Possibly the [Fourfold God](../Other/Fourfold%20God.md) he mentioned.
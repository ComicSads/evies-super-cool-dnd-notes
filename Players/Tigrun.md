---
aliases:
  - Sara
Class: Barbarian
pronouns: he/him
---
A male ogre who is unusually well dressed with a long braid on his back and stomach. He carries a massive log on his back which he uses as a walking staff.
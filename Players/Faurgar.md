---
aliases:
  - John
Class: Hunter
---
Heavily armored man dressed in scale mail who uses a great sword. He is a member of the [Monster Hunter guild](../Notes/Other/Monster%20Hunter%20guild.md) and carries a book on his hip.
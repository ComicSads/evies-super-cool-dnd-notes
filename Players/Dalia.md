---
aliases:
  - Evie
Class: Bard
pronouns: she/her
---
If u aren't Evie or CAX and u read this u r cringe

Raised mostly by her mother as her dad went missing when she was a baby. Her mother sung to her often as she was growing until Dalia's horns started to sprout when her mother beat her, threw her out, and told her never to return. Dalia started running from her home town with no idea where to turn as her skin turned more crimson in the falling rain. While running she stumbled into the fae realm and found The Bastion of Song, a group of musically inclined Satyr's who took her in and raised her. Each day was a different genre of musical production almost. Here Dalia learned how to perform with her lute